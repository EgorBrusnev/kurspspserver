package com.kurs.psp.login

import com.kurs.psp.controller.LoginController
import com.kurs.psp.controller.MainController
import com.kurs.psp.utils.PREF_HEIGHT
import com.kurs.psp.utils.PREF_WIDTH
import com.kurs.psp.view.LoginView
import com.kurs.psp.view.admin.AdminMainView
import javafx.animation.Interpolator
import javafx.application.Platform
import javafx.beans.binding.BooleanExpression
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.value.ObservableBooleanValue
import javafx.geometry.Insets
import javafx.scene.Node
import javafx.scene.Parent
import javafx.stage.Stage
import javafx.util.Duration
import tornadofx.*
import kotlin.concurrent.thread

class LoginApp : App(LoginView::class, Styles::class) {
    private val loginController: LoginController by inject()

    override fun start(stage: Stage) {
        super.start(stage)
        loginController.init()
    }


}

class TestApp : App(MyWorkspace::class) {
    private val mainController: LoginController by inject()

    override fun onBeforeShow(view: UIComponent) {
        workspace.headingContainer.hide()
        workspace.header.hide()
        workspace.deleteButton.hide()
        workspace.refreshButton.hide()
        workspace.createButton.hide()
        workspace.saveButton.hide()
        mainController.init()
    }
}

class MyWorkspace : Workspace("dasdsa", navigationMode = NavigationMode.Stack) {
    init {
        root.prefWidth = PREF_WIDTH
        root.prefHeight = PREF_HEIGHT
    }
}