package com.kurs.psp

import com.kurs.psp.utils.SocketConnector
import com.psp.kurs.server.model.KursApi
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.net.ConnectException
import java.net.Socket


object ClientSocket {

    private var socket: Socket? = null
    private var oos: ObjectOutputStream? = null
    private var ois: ObjectInputStream? = null

    init {
        try {
            socket = SocketConnector.getSocket()?.also {
                oos = ObjectOutputStream(it.getOutputStream())
                ois = ObjectInputStream(it.getInputStream())
            }
        } catch (e: ConnectException) {
            println("Error: ${e.localizedMessage}")
        }
    }

    fun isConnected() = socket != null

    fun send(func: KursApi.Function): KursApi.Object? {
        oos?.writeObject(func)
        oos?.flush()
        val res = ois?.readObject()
        println("get response $res")
        return res as KursApi.Object?
    }
}