package com.kurs.psp.model

import com.kurs.psp.utils.Education
import com.kurs.psp.utils.EnglishLevel
import com.kurs.psp.utils.UserRoles
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import tornadofx.getValue
import tornadofx.setValue

class User() : ViewModel() {

    constructor(
        username: String,
        surname: String,
        name: String,
        patronymic: String,
        age: Int,
        groupNumber: Int,
        englishLevel: EnglishLevel,
        education: Education,
        role: UserRoles,
        id: Int = -1
    ) : this() {
        this.surname = surname
        this.name = name
        this.patronymic = patronymic
        this.age = age
        this.education = education
        this.englishLevel = englishLevel
        this.groupNumber = groupNumber
        this.username = username
        this.role = role
        this.id = id
    }

    var id: Int = -1


    val nameProperty = SimpleStringProperty()
    var name by nameProperty

    val surnameProperty = SimpleStringProperty()
    var surname by surnameProperty

    val patronymicProperty = SimpleStringProperty()
    var patronymic by patronymicProperty

    val ageProperty = SimpleIntegerProperty()
    var age by ageProperty

    val educationProperty = SimpleObjectProperty<Education>()
    var education by educationProperty

    val englishLevelProperty = SimpleObjectProperty<EnglishLevel>()
    var englishLevel by englishLevelProperty

    val groupNumberProperty = SimpleIntegerProperty()
    var groupNumber by groupNumberProperty

    val usernameProperty = SimpleStringProperty()
    var username by usernameProperty

    val roleProperty = SimpleObjectProperty<UserRoles>()
    var role by roleProperty


}

class UserModel : ItemViewModel<User>() {
    val id = bind(User::id)
    val name = bind(User::name)
    val surname = bind(User::surname)
    val patronymic = bind(User::patronymic)
    val age = bind(User::age)
    val education = bind(User::education)
    val englishLevel = bind(User::englishLevel)
    val groupNumber = bind(User::groupNumber)
    val username = bind(User::username)
}
