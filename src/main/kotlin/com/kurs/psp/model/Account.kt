package com.kurs.psp.model

import com.kurs.psp.utils.UserRoles
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*

class Account(
) : ViewModel() {
    val usernameProperty = SimpleStringProperty()
    var username by usernameProperty

    val passwordProperty = SimpleStringProperty()
    var password by passwordProperty

    val roleProperty = SimpleObjectProperty<UserRoles>(UserRoles.USER)
    var role by roleProperty


    constructor(username: String, password: String, role: UserRoles) : this() {
        this.username = username
        this.password = password
        this.role = role
    }
}

class AccountModel : ItemViewModel<Account>() {
    val username = bind(Account::usernameProperty)
    val password = bind(Account::passwordProperty)
    val role = bind(Account::roleProperty)
}
