package com.kurs.psp.model

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*

class Group {

    constructor(id: Int, lang: String, users: List<User>) {
        this.id = id;
        this.language = lang
        this.users = users
    }

    val idProperty = SimpleIntegerProperty()
    var id by idProperty

    val languageProperty = SimpleStringProperty()
    var language by languageProperty

    val usersProperty = SimpleObjectProperty<List<User>>()
    var users by usersProperty
}

class GroupModel : ItemViewModel<Group>() {
    val id = bind(Group::idProperty)
    val language = bind(Group::languageProperty)
    val users = bind(Group::usersProperty)
}
