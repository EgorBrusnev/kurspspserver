package com.kurs.psp.model

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import java.time.LocalDateTime

data class Schedule(
    val lesson: String = "english",
    val groupId: Int = 0,
    val teacher: String = "teacher",
    val room: String = "307",
    val startAt: LocalDateTime,
    val endAt: LocalDateTime
)

class ScheduleModel : ItemViewModel<Schedule>() {
    val lesson = bind(Schedule::lesson)
    val groupId = bind(Schedule::groupId)
    val teacher = bind(Schedule::teacher)
    val room = bind(Schedule::room)
    val startAt = bind(Schedule::startAt)
    val endAt = bind(Schedule::endAt)
}


//class Lesson {
//    val groupIdProperty = SimpleIntegerProperty()
//    var groupId by groupIdProperty
//
//    val teacherProperty = SimpleStringProperty()
//    var teacher by teacherProperty
//
//    val roomProperty = SimpleIntegerProperty()
//    var room by roomProperty
//
//    val startAtProperty = SimpleObjectProperty<LocalDateTime>()
//    var startAt by startAtProperty
//
//    val endAtProperty = SimpleObjectProperty<LocalDateTime>()
//    var endAt by endAtProperty
//}