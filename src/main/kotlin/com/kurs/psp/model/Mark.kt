package com.kurs.psp.model

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import java.time.LocalDateTime
import tornadofx.getValue
import tornadofx.setValue
import java.time.LocalDate

class Mark() : ViewModel() {

    constructor(student: String, mark: Int, date: LocalDate) : this() {
        this.student = student
        this.mark = mark
        this.date = date
    }

    val dateProperty = SimpleObjectProperty<LocalDate>()
    var date by dateProperty
    val markProperty = SimpleIntegerProperty()
    var mark by markProperty
    val studentProperty = SimpleStringProperty()
    var student by studentProperty
}

class MarkList() : ViewModel() {

    constructor(student: String, marks: List<Mark>) : this() {
        this.student = student
        this.marks = marks
    }

    val studentProperty = SimpleStringProperty()
    var student by studentProperty

    val marksProperty = SimpleObjectProperty<List<Mark>>()
    var marks by marksProperty

}

class MarkListModel : ItemViewModel<MarkList>() {
    val student = bind(MarkList::student)
    val marks = bind(MarkList::marks)
}


class MarkModel : ItemViewModel<Mark>() {
    val student = bind(Mark::student)
    val mark = bind(Mark::mark)
    val date = bind(Mark::date)
}
