package com.kurs.psp.utils

import java.net.ConnectException
import java.net.InetAddress
import java.net.Socket

object SocketConnector {
    private var socket: Socket? = null

    init {
        try {
            val inetAddress = InetAddress.getByName("localhost")
            socket = Socket(inetAddress, 8000)
            println("Connected to server!")
        } catch (e: ConnectException) {
            println("Error on connect ${e.localizedMessage}")
        }
    }

    fun getSocket() = socket

}