package com.kurs.psp.utils

enum class UserRoles {
    ADMIN, USER, TEACHER, ALL
}

enum class Education {
    HIGH, MIDDLE, BASE
}

enum class EnglishLevel {
    UPPER_INTERMEDIATE, INTERMEDIATE, PRE_INTERMEDIATE
}

const val PREF_WIDTH = 1000.0
const val PREF_HEIGHT = 500.0
const val TOKEN = "token"
const val PREFERENCE = "login_preference"