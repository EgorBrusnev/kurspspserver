package com.kurs.psp.controller

import com.kurs.psp.ClientSocket
import com.kurs.psp.model.Mark
import com.kurs.psp.model.User
import com.kurs.psp.utils.Education
import com.kurs.psp.utils.EnglishLevel
import com.kurs.psp.utils.UserRoles
import com.psp.kurs.server.model.KursApi
import tornadofx.*

class StatisticController : Controller() {

    fun getMarksForUser(username: String): List<Mark> = ClientSocket.send(KursApi.GetMarksByUsername(username))
        .let {
            when (it) {
                is KursApi.MarkList -> it.marks.map { Mark(it.username, it.value, it.date) }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                    emptyList()
                }
                else -> {
                    println("Unknown response $it")
                    emptyList()
                }
            }
        }

    fun getMarksForGroup(group: Int): List<Mark> = ClientSocket.send(KursApi.GetMarksForGroup(group))
        .let {
            when (it) {
                is KursApi.MarkList -> {
                    it.marks.map { Mark(it.username, it.value, it.date) }
                }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                    emptyList()
                }
                else -> {
                    println("Unknown reponse $it")
                    emptyList()
                }
            }
        }

    fun getUsersForGroup(group: Int): List<User> = ClientSocket.send(KursApi.GetUsersForGroup(group))
        .let {
            when (it) {
                is KursApi.UserList -> {
                    it.users.map {
                        User(
                            it.username,
                            it.surname,
                            it.name,
                            it.patronymic,
                            it.age,
                            it.groupNumber,
                            EnglishLevel.valueOf(it.englishLevel),
                            Education.valueOf(it.education),
                            UserRoles.valueOf(it.role)
                        )
                    }
                }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                    emptyList()
                }
                else -> {
                    println("Unknown reponse $it")
                    emptyList()
                }
            }
        }

    fun addMark(mark: Mark) =
        ClientSocket.send(KursApi.AddMark(KursApi.Mark(username = mark.student, value = mark.mark, date = mark.date)))
}