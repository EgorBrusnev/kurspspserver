package com.kurs.psp.controller

import com.kurs.psp.ClientSocket
import com.kurs.psp.model.AccountModel
import com.kurs.psp.model.UserModel
import com.kurs.psp.utils.PREFERENCE
import com.kurs.psp.utils.TOKEN
import com.kurs.psp.utils.UserRoles
import com.kurs.psp.view.UserPageView
import com.kurs.psp.view.admin.AdminMainView
import com.psp.kurs.server.model.KursApi
import tornadofx.*

class RegistrationController : Controller() {

    fun doRegister(account: AccountModel, user: UserModel) {
        runAsync {
            ClientSocket.send(
                KursApi.RegisterUser(
                    KursApi.Account(
                        account.username.value,
                        account.password.value,
                        UserRoles.USER.name
                    ),
                    KursApi.User(
                        user.id.value,
                        account.username.value,
                        user.surname.value,
                        user.name.value,
                        user.patronymic.value,
                        user.age.value,
                        user.groupNumber.value,
                        user.education.value.name,
                        user.englishLevel.value.name,
                        UserRoles.USER.name
                    )

                )
            )
        } ui {
            if (it is KursApi.Error)
                println("Error: ${it.error}")
            else if (it is KursApi.AuthenticationResult) {
                if (it.isSuccess) {
                    preferences(PREFERENCE) {
                        put(TOKEN, it.token)
                    }
                    when (it.role) {
                        UserRoles.ADMIN.name -> workspace.dock<AdminMainView>()
                        UserRoles.USER.name -> workspace.dock<UserPageView>()
                        UserRoles.TEACHER.name -> workspace.dock<AdminMainView>()
                        else -> println("Unknown role")
                    }
                } else {
                    println("un success")
                }
            }
        }
    }
}