package com.kurs.psp.controller

import com.kurs.psp.ClientSocket
import com.kurs.psp.model.User
import com.kurs.psp.view.ProfileView
import com.psp.kurs.server.model.KursApi
import tornadofx.*

class ProfileController : Controller() {

    private val view: ProfileView by inject()

    fun updateUser(user: User) {
        ClientSocket.send(
            KursApi.UpdateUsers(
                KursApi.UserList(
                    listOf(
                        KursApi.User(
                            user.id,
                            user.username,
                            user.surname,
                            user.name,
                            user.patronymic,
                            user.age,
                            user.groupNumber,
                            user.education.name,
                            user.englishLevel.name,
                            user.role.name
                        )
                    )
                )
            )
        ).let {
            when (it) {
                is KursApi.Ok -> {
                    println("ok")
                    view.updateUser()
                }
                is KursApi.Error -> println("Error: ${it.error}")
            }
        }
    }

}