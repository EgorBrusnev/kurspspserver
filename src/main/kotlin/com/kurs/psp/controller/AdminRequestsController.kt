package com.kurs.psp.controller

import com.kurs.psp.ClientSocket
import com.kurs.psp.model.Group
import com.kurs.psp.model.User
import com.kurs.psp.utils.Education
import com.kurs.psp.utils.EnglishLevel
import com.kurs.psp.utils.UserRoles
import com.kurs.psp.view.admin.AdminRequestsView
import com.psp.kurs.server.model.KursApi
import tornadofx.*

class AdminRequestsController : Controller() {

    private val view: AdminRequestsView by inject()

    fun getRequests(): List<User> = ClientSocket.send(KursApi.GetUnRegisteredUsers()).let {
        when (it) {
            is KursApi.UserList -> {
                it.users.map {
                    User(
                        it.username,
                        it.surname,
                        it.name,
                        it.patronymic,
                        it.age,
                        it.groupNumber,
                        EnglishLevel.valueOf(it.englishLevel),
                        Education.valueOf(it.education),
                        UserRoles.valueOf(it.role)

                    )
                }
            }
            is KursApi.Error -> {
                println("Error: ${it.error}")
                emptyList()
            }
            else -> {
                println("Unknown method")
                emptyList()
            }
        }
    }

    fun setUserGroup(username: String, group: Int) {
        ClientSocket.send(KursApi.SetUserGroup(username, group)).let {
            when (it) {
                is KursApi.Ok -> {
                    println("Ok")
                    view.refreshTable()
                }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                }
                else -> {
                    println("Unknown method")
                }
            }
        }
    }

    fun getAllGroups(): List<Group> = ClientSocket.send(KursApi.GetAllGroups())
        .let {
            when (it) {
                is KursApi.GroupList -> {
                    it.groups.map {
                        Group(
                            it.id,
                            it.lang,
                            it.students.map {
                                User(
                                    it.username,
                                    it.surname,
                                    it.name,
                                    it.patronymic,
                                    it.age,
                                    it.groupNumber,
                                    EnglishLevel.valueOf(it.englishLevel),
                                    Education.valueOf(it.education),
                                    UserRoles.valueOf(it.role),
                                    it.id
                                )
                            }
                        )
                    }
                }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                    emptyList()
                }
                else -> {
                    println("Unknown method")
                    emptyList()
                }
            }
        }
}