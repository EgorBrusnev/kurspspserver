package com.kurs.psp.controller

import com.kurs.psp.ClientSocket
import com.kurs.psp.model.AccountModel
import com.kurs.psp.model.User
import com.kurs.psp.model.UserModel
import com.kurs.psp.utils.Education
import com.kurs.psp.utils.EnglishLevel
import com.kurs.psp.utils.UserRoles
import com.kurs.psp.view.UserListView
import com.psp.kurs.server.model.KursApi
import tornadofx.*

class UserListController : Controller() {

    private val userlistView: UserListView by inject()

    fun submitNewAccount(account: AccountModel, user: UserModel) {
        ClientSocket.send(
            KursApi.RegisterUser(
                KursApi.Account(
                    account.username.value,
                    account.password.value,
                    UserRoles.USER.name
                ),
                KursApi.User(
                    user.id.value,
                    account.username.value,
                    user.surname.value,
                    user.name.value,
                    user.patronymic.value,
                    user.age.value,
                    user.groupNumber.value,
                    user.education.value.name,
                    user.englishLevel.value.name,
                    UserRoles.USER.name
                )

            )
        ).also {
            when (it) {
                is KursApi.AuthenticationResult -> {
                    if (it.isSuccess) {
                        println("Success")
                        onSuccess()
                    } else {
                        println("Not success")
                    }
                }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                }
                else -> {
                    println("Unknown response $it")

                }
            }
        }
    }

    private fun onSuccess() {
        userlistView.updateUsers()
    }

    fun getUsersByRole(role: UserRoles): List<User> = ClientSocket.send(KursApi.GetUsersByRole(role.name)).let {
        when (it) {
            is KursApi.UserList -> {
                it.users.map {
                    User(
                        it.username,
                        it.surname,
                        it.name,
                        it.patronymic,
                        it.age,
                        it.groupNumber,
                        EnglishLevel.valueOf(it.englishLevel),
                        Education.valueOf(it.education),
                        UserRoles.valueOf(it.role),
                        it.id
                    )
                }
            }
            is KursApi.Error -> {
                println("Error: ${it.error}")
                emptyList()
            }
            else -> {
                println("Unknown response $it")
                emptyList()
            }
        }
    }

    fun updateUsers(users: List<User>) {
        ClientSocket.send(
            KursApi.UpdateUsers(
                KursApi.UserList(
                    users.map {
                        KursApi.User(
                            id = it.id,
                            username = it.username,
                            surname = it.surname,
                            name = it.name,
                            patronymic = it.patronymic,
                            age = it.age,
                            role = it.role.name,
                            groupNumber = it.groupNumber,
                            education = it.education.name,
                            englishLevel = it.englishLevel.name
                        )
                    }
                )
            )
        ).let {
            when (it) {
                is KursApi.Ok -> {
                    println("Ok update")
                    onSuccess()
                }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                }
            }
        }
    }

    fun deleteUser(id: Int) {
        ClientSocket.send(KursApi.DeleteUser(id)).let {
            when (it) {
                is KursApi.Ok -> {
                    println("Ok delete")
                    onSuccess()
                }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                }
            }
        }
    }

    fun getAllUsers() = ClientSocket.send(KursApi.GetAllUsers()).let {
        when (it) {
            is KursApi.UserList -> {
                it.users.map {
                    User(
                        it.username,
                        it.surname,
                        it.name,
                        it.patronymic,
                        it.age,
                        it.groupNumber,
                        EnglishLevel.valueOf(it.englishLevel),
                        Education.valueOf(it.education),
                        UserRoles.valueOf(it.role),
                        it.id
                    )
                }
            }
            is KursApi.Error -> {
                println("Error: ${it.error}")
                emptyList()
            }
            else -> {
                println("Unknown response: $it")
                emptyList()
            }
        }
    }
}