package com.kurs.psp.controller

import com.kurs.psp.ClientSocket
import com.kurs.psp.view.LoginView
import com.kurs.psp.utils.PREFERENCE
import com.kurs.psp.utils.TOKEN
import com.kurs.psp.utils.UserRoles
import com.kurs.psp.view.RegistrationView1
import com.kurs.psp.view.UserPageView
import com.kurs.psp.view.admin.AdminMainView
import com.psp.kurs.server.model.KursApi
import tornadofx.*


class LoginController : Controller() {

    private val loginScreen: LoginView by inject()


    fun init() {
        preferences(PREFERENCE) {
            get(TOKEN, "")?.let {
                tryLogin(token = it, remember = true)
            } ?: showLoginScreen("Please log in")
        }
    }

    private fun showLoginScreen(message: String, shake: Boolean = false) {
        scope.workspace.dock<LoginView>()
        runLater { if (shake) loginScreen.shakeStage() }
    }

    private fun showSecureScreen() {
        loginScreen.workspace.dock<AdminMainView>()
    }

    private fun showUserScreen() {
        loginScreen.workspace.dock<UserPageView>()
    }

    fun showRegistration() {
        loginScreen.replaceWith<RegistrationView1>()
    }

    fun tryLogin(username: String? = null, password: String? = null, token: String? = null, remember: Boolean) {
        takeIf { ClientSocket.isConnected() }?.let {
            runAsync {
                (token
                    ?.let { ClientSocket.send(KursApi.SendLoginToken(token)) }
                    ?: ClientSocket.send(KursApi.SendLoginRequest(username, password))) as KursApi.AuthenticationResult
            } ui { response ->
                if (response.isSuccess) {
                    loginScreen.clear()
                    if (remember) {
                        preferences(PREFERENCE) {
                            put(TOKEN, response.token)
                        }
                    }
                    println("Role: ${response.role}")
                    if (response.role == UserRoles.ADMIN.name)
                        showSecureScreen()
                    else
                        showUserScreen()
                } else {
                    showLoginScreen("Login failed", true)
                }
            }
        }
    }

    fun logout() {
        preferences(PREFERENCE) {
            remove(TOKEN)
        }
        showLoginScreen("Log in as another user")
    }

}