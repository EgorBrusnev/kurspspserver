package com.kurs.psp.controller

import com.kurs.psp.utils.PREFERENCE
import com.kurs.psp.utils.TOKEN
import com.kurs.psp.view.LoginView
import com.kurs.psp.view.admin.AdminMainView
import tornadofx.*

class AdminController : Controller() {

    private val view: AdminMainView by inject()

    fun logout() {
        preferences(PREFERENCE) {
            remove(TOKEN)
        }
        view.replaceWith<LoginView>()
    }
}