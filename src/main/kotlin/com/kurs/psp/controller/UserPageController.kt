package com.kurs.psp.controller

import com.kurs.psp.utils.PREFERENCE
import com.kurs.psp.utils.TOKEN
import com.kurs.psp.view.*
import tornadofx.*

class UserPageController : Controller() {
    private val userPageView: UserPageView by inject()

    fun logout() {
        preferences(PREFERENCE) {
            remove(TOKEN)
        }
        userPageView.replaceWith<LoginView>()
    }

    fun showProfile() {
        userPageView.workspace.dock<ProfileView>()
    }

    fun showSchedule() {
        userPageView.workspace.dock<ScheduleView>()
    }

    fun showMarks(username: String) {
        userPageView.workspace.dock<StatisticView>(params = *arrayOf(Pair("user", username)))
    }
}