package com.kurs.psp.controller

import com.kurs.psp.ClientSocket
import com.kurs.psp.model.Schedule
import com.kurs.psp.model.ScheduleModel
import com.kurs.psp.view.ScheduleView
import com.psp.kurs.server.model.KursApi
import tornadofx.*

class ScheduleController : Controller() {

    private val scheduleView: ScheduleView by inject()

    fun getLessons(groupId: Int): List<Schedule> = ClientSocket.send(KursApi.GetScheduleByGroup(groupId))
        .let {
            when (it) {
                is KursApi.ScheduleList ->
                    it.lessons.map { Schedule(it.lesson, it.groupId, it.teacher, it.room, it.startAt, it.endAt) }

                is KursApi.Error -> {
                    println("Error: ${it.error}")
                    emptyList()
                }
                else -> {
                    println("Unknown response $it")
                    emptyList()
                }
            }
        }

    fun addNewSchedule(schedule: ScheduleModel) {
        ClientSocket.send(
            KursApi.AddNewSchedule(
                KursApi.Schedule(
                    lesson = schedule.lesson.value,
                    groupId = schedule.groupId.value,
                    room = schedule.room.value,
                    teacher = schedule.teacher.value,
                    startAt = schedule.startAt.value,
                    endAt = schedule.endAt.value
                )
            )
        ).let {
            when (it) {
                is KursApi.Ok -> {
                    println("Ok adding schedule")
                    scheduleView.refresh()
                }
                is KursApi.Error -> {
                    println("Error: ${it.error}")
                }
                else -> {
                    println("Unknown response $it")
                }
            }
        }
    }
}