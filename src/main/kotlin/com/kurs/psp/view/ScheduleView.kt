package com.kurs.psp.view

import com.kurs.psp.controller.ScheduleController
import com.kurs.psp.controller.UserListController
import com.kurs.psp.model.Schedule
import com.kurs.psp.model.ScheduleModel
import com.kurs.psp.model.User
import com.kurs.psp.utils.UserRoles
import javafx.event.EventHandler
import javafx.util.converter.IntegerStringConverter
import jfxtras.scene.control.agenda.Agenda
import tornadofx.*
import javafx.beans.property.ObjectProperty
import javafx.scene.input.KeyCode
import java.time.format.DateTimeFormatter
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import java.time.chrono.IsoChronology
import java.time.format.DateTimeFormatterBuilder
import javafx.scene.control.DatePicker
import javafx.scene.input.KeyEvent
import javafx.util.StringConverter
import jfxtras.internal.scene.control.fxml.CalendarTimePickerBuilder
import jfxtras.scene.control.*
import tornadofx.control.DateTimePicker
import java.text.DateFormat
import java.time.*
import java.time.format.FormatStyle
import java.util.*


class ScheduleView : SecureView("Расписания") {

    private val scheduleController: ScheduleController by inject()

    private var lessons: List<Schedule> = emptyList()

    private var dateWhen = LocalDateTime.now()

    private val agenda = Agenda()

    private var group: Int = -1

    init {
        currentUser?.let {
            if (it.role == UserRoles.USER) {
                lessons = scheduleController.getLessons(it.groupNumber)
                refresh()
            }
        }
    }

    fun refresh() {
        if (group != -1) {
            lessons = scheduleController.getLessons(group)
            agenda.apply {
                displayedLocalDateTime = dateWhen
                allowDragging = false
                allowResize = false
                appointments().apply {
                    setAll(lessons.map {
                        Agenda.AppointmentImplLocal()
                            .withLocation(it.room)
                            .withSummary(it.lesson)
                            .withDescription("Учитель: ${it.teacher}")
                            .withStartLocalDateTime(it.startAt)
                            .withEndLocalDateTime(it.endAt)
                            .withAppointmentGroup(Agenda.AppointmentGroupImpl().withStyleClass("group1"))
                    })
                }
            }
        }
    }

    override val root = borderpane {
        right {
            vbox {
                if (currentUser?.role != UserRoles.USER) {
                    label("Группа")
                    textfield {
                        promptText = "Группа"
                        action {
                            group = text.toInt()
//                            lessons = scheduleController.getLessons(text.toInt())
                            refresh()
                        }
                    }
                }
                label("Начиная с:")
                datepicker {
                    onAction = EventHandler {
                        dateWhen = value.atTime(8, 0)
                        agenda.displayedLocalDateTime = dateWhen

                    }
                }
                if (currentUser?.role != UserRoles.USER) {
                    button("Добавить урок") {
                        action {
                            AddLessonDialog().apply {
                                setOnOkListener {
                                    if (it.isValid) {
                                        scheduleController.addNewSchedule(it)
                                    }
                                }
                            }.openModal()
                        }
                    }
                }
            }
            center {
                add(agenda)
            }
        }
    }

    class AddLessonDialog : View("Добавление урока") {

        private val startDateTimePicker = CalendarTextField().apply {
            showTime = true
            dateFormat = DateFormat.getDateTimeInstance()
        }
        private val endDateTimePicker = CalendarTextField().apply {
            showTime = true
            dateFormat = DateFormat.getDateTimeInstance()
        }
        private val schedule: ScheduleModel by inject()
        private var onOk: (ScheduleModel) -> Unit = {}

        private val userListController: UserListController by inject()

        private val teachers =
            FXCollections.observableArrayList<User>(*userListController.getUsersByRole(UserRoles.TEACHER).toTypedArray())

        override val root = borderpane {
            prefWidth = 500.0
            center {
                vbox {
                    label("Предмет")
                    textfield(schedule.lesson) {
                        required()
                    }
                    label("Группа")
                    textfield(schedule.groupId, IntegerStringConverter()) {
                        required()
                    }
                    label("Учитель")
                    combobox(schedule.teacher) {
                        items = teachers.map { it.username }.observable()
                        required()

                    }
                    label("Аудитория")
                    textfield(schedule.room) {
                        required()
                    }
                    label("Начинается в:")
                    add(startDateTimePicker)
                    label("Заканчивается:")
                    add(endDateTimePicker)
                    button("Ok") {
                        action {
                            schedule.startAt.value = startDateTimePicker.getLocalDateTime()
                            schedule.endAt.value = endDateTimePicker.getLocalDateTime()
                            close()
                            onOk(schedule)
                        }
                    }
                }
            }
        }

        private fun CalendarTextField.getLocalDateTime() =
            LocalDateTime.ofInstant(Instant.ofEpochMilli(Date.parse(text)), ZoneId.systemDefault())

        fun setOnOkListener(block: (ScheduleModel) -> Unit) {
            onOk = block
        }
    }

    fun a() {

    }


//    class DateTimePicker : DatePicker() {
//
//        private var formatter = DateTimeFormatter.ofPattern(DefaultFormat)
//
//        private val dateTimeValue = SimpleObjectProperty(LocalDateTime.now())
//
//        private val format = object : SimpleObjectProperty<String>() {
//            override fun set(newValue: String) {
//                super.set(newValue)
//                formatter = DateTimeFormatter.ofPattern(newValue)
//            }
//        }
//
//        init {
//            styleClass.add("datetime-picker")
//            converter = InternalConverter()
//
//            // Syncronize changes to the underlying date value back to the
//            // dateTimeValue
//            valueProperty().addListener { observable, oldValue, newValue ->
//                if (newValue == null) {
//                    dateTimeValue.set(null)
//                } else {
//                    if (dateTimeValue.get() == null) {
//                        dateTimeValue.set(LocalDateTime.of(newValue, LocalTime.now()))
//                    } else {
//                        val time = dateTimeValue.get().toLocalTime()
//                        dateTimeValue.set(LocalDateTime.of(newValue, time))
//                    }
//                }
//            }
//
//            // Syncronize changes to dateTimeValue back to the underlying date value
//            dateTimeValue.addListener { observable, oldValue, newValue -> value = newValue?.toLocalDate() }
//
//            // Persist changes onblur
//            editor.focusedProperty().addListener { observable, oldValue, newValue ->
//                if (!newValue)
//                    simulateEnterPressed()
//            }
//
//        }
//
//        private fun simulateEnterPressed() {
//            editor.fireEvent(
//                KeyEvent(
//                    editor, editor, KeyEvent.KEY_PRESSED, null, null, KeyCode.ENTER,
//                    false, false, false, false
//                )
//            )
//        }
//
//        fun getDateTimeValue(): LocalDateTime? {
//            return dateTimeValue.get()
//        }
//
//        fun setDateTimeValue(dateTimeValue: LocalDateTime) {
//            if (dateTimeValue.isAfter(LocalDateTime.of(1971, 6, 30, 12, 0)))
//                this.dateTimeValue.set(dateTimeValue)
//            else
//                this.dateTimeValue.set(null)
//        }
//
//        fun dateTimeValueProperty(): ObjectProperty<LocalDateTime> {
//            return dateTimeValue
//        }
//
//        fun getFormat(): String {
//            return format.get()
//        }
//
//        fun formatProperty(): ObjectProperty<String> {
//            return format
//        }
//
//        fun setFormat(format: String) {
//            this.format.set(format)
//        }
//
//        internal inner class InternalConverter : StringConverter<LocalDate>() {
//            override fun toString(`object`: LocalDate?): String {
//
//                val value = getDateTimeValue()
//                return if (value != null) value.format(formatter) else ""
//            }
//
//            override fun fromString(value: String?): LocalDate? {
//                if (value == null || value.isEmpty()) {
//                    dateTimeValue.set(null)
//                    return null
//                }
//
//                dateTimeValue.set(LocalDateTime.parse(value, formatter))
//                return dateTimeValue.get().toLocalDate()
//            }
//        }
//
//        companion object {
//            val DefaultFormat = DateTimeFormatterBuilder.getLocalizedDateTimePattern(
//                FormatStyle.MEDIUM, FormatStyle.SHORT, IsoChronology.INSTANCE,
//                Locale.GERMANY
//            ) // or whatever Locale
//        }
//    }
}
