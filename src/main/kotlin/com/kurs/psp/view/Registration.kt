package com.kurs.psp.view

import com.kurs.psp.controller.RegistrationController
import com.kurs.psp.model.Account
import com.kurs.psp.model.AccountModel
import com.kurs.psp.model.User
import com.kurs.psp.model.UserModel
import com.kurs.psp.utils.Education
import com.kurs.psp.utils.EnglishLevel
import com.kurs.psp.utils.PREF_HEIGHT
import com.kurs.psp.utils.PREF_WIDTH
import com.kurs.psp.utils.UserRoles
import javafx.collections.FXCollections
import javafx.util.converter.IntegerStringConverter
import tornadofx.*

class RegistrationView1 : Wizard("Регистрация") {

    override val canGoNext = currentPageComplete
    override val canFinish = allPagesComplete

    private val registrationController: RegistrationController by inject()
    private val account: AccountModel by inject()
    private val user: UserModel by inject()
    private var onSave: ((AccountModel, UserModel) -> Unit)? = null

    init {
        root.prefWidth = PREF_WIDTH
        root.prefHeight = PREF_HEIGHT
        add<RegisterAccount>()
        add<RegisterUserView>()
    }

    override fun onSave() {
        println("on save wizard ${account.username} ${user.surname}")
        onSave?.run {
            invoke(account, user)
        } ?: registrationController.doRegister(account, user)
    }


    fun setOnSaveListener(f: (AccountModel, UserModel) -> Unit) {
        onSave = f
    }
}

class RegisterAccount : View("Создание аккаунта") {

    private val accountModel: AccountModel by inject()

    override val complete = accountModel.valid

    override val root = form {
        fieldset {
            field("Имя пользователя") {
                textfield(accountModel.username) {
                    required()
                }
            }
            field("Пароль") {
                passwordfield(accountModel.password) {
                    required()
                }
            }
        }
    }
}

class RegisterUserView : View("Информация о пользователе") {
    val userModel: UserModel by inject()

    override val complete = userModel.valid

    override val root = form {
        fieldset {
            field("Фамилия") {
                textfield(userModel.surname) {
                    required()
                }
            }
            field("Имя") {
                textfield(userModel.name) {
                    required()
                }
            }
            field("Отчество") {
                textfield(userModel.patronymic) {
                    required()
                }
            }
            field("Возраст") {
                textfield(userModel.age, IntegerStringConverter()) {
                    required()
                }
            }
            field("Образование") {
                combobox<Education>(userModel.education) {
                    value = Education.BASE
                    items = FXCollections.observableArrayList(*Education.values())
                    required()
                }
            }
            field("Уровень английского") {
                combobox<EnglishLevel>(userModel.englishLevel) {
                    value = EnglishLevel.INTERMEDIATE
                    items = FXCollections.observableArrayList(*EnglishLevel.values())
                    required()
                }
            }
        }
    }
}