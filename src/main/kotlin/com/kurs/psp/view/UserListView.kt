package com.kurs.psp.view

import com.kurs.psp.controller.UserListController
import com.kurs.psp.model.AccountModel
import com.kurs.psp.model.User
import com.kurs.psp.model.UserModel
import com.kurs.psp.utils.*
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.geometry.Pos
import javafx.scene.control.TableView
import javafx.scene.control.cell.ComboBoxTableCell
import javafx.util.StringConverter
import tornadofx.*

class UserListView : View("My View") {

    private val userListController: UserListController by inject()

    private val users = FXCollections.observableArrayList<User>(userListController.getUsersByRole(UserRoles.USER))

    private var table = TableView<User>()

    private val updatedUsers = arrayListOf<User>()

    private val displayRole = SimpleObjectProperty<UserRoles>(UserRoles.USER)

    fun updateUsers() {
        users.clear()
        if (displayRole != UserRoles.ALL) {
            users.addAll(userListController.getUsersByRole(displayRole.value))
        } else {
            users.addAll(userListController.getAllUsers())
        }
        table.refresh()
    }

    override val root = vbox {
        prefHeight = PREF_HEIGHT
        prefWidth = PREF_WIDTH
        alignment = Pos.CENTER
        hbox {
            alignment = Pos.CENTER
            table = tableview(users) {
                onEditCommit {
                    updatedUsers.indexOfFirst { it.id == rowValue.id }.takeIf { it != -1 }?.let {
                        updatedUsers.set(it, rowValue)
                    } ?: updatedUsers.add(rowValue)
                    updatedUsers
                }
                columnResizePolicy = SmartResize.POLICY
                isEditable = true
                column("Имя", User::name) {
                    makeEditable()
                }
                column("Фамилия", User::surname).makeEditable()
                column("Отчество", User::patronymic).makeEditable()
                column("Возраст", User::age).makeEditable()
                column("Образование", User::education)
                    .setCellFactory { ComboBoxTableCell(EducationConverter(), *Education.values()) }
                column("Уровень английского", User::englishLevel)
                    .setCellFactory { ComboBoxTableCell(EnglishLevelConverter(), *EnglishLevel.values()) }
                column("Роль", User::role)
                    .setCellFactory { ComboBoxTableCell(UserRoleConverter(), *UserRoles.values()) }
            }
        }
        hbox {
            buttonbar {
                button("Создать") {
                    action {
                        RegistrationView1().apply {
                            setOnSaveListener { acc: AccountModel, usr: UserModel ->
                                userListController.submitNewAccount(acc, usr)
                                close()
                            }
                            openModal()
                        }
                    }
                }
                button("Обновить") {
                    action {
                        userListController.updateUsers(updatedUsers)
                    }
                }
                button("Удалить") {
                    action {
                        userListController.deleteUser(table.selectionModel.selectedItem.id)
                    }
                }
            }
            combobox(displayRole) {
                items = FXCollections.observableArrayList(*UserRoles.values())
                setOnAction {
                    updateUsers()
                }
            }
        }
    }

}

class EducationConverter : StringConverter<Education>() {
    override fun toString(edu: Education): String = edu.name

    override fun fromString(p0: String): Education = Education.valueOf(p0)
}

class EnglishLevelConverter : StringConverter<EnglishLevel>() {
    override fun toString(p0: EnglishLevel): String = p0.name

    override fun fromString(p0: String): EnglishLevel = EnglishLevel.valueOf(p0)
}

class UserRoleConverter : StringConverter<UserRoles>() {
    override fun toString(p0: UserRoles): String = p0.name

    override fun fromString(p0: String): UserRoles = UserRoles.valueOf(p0)
}
