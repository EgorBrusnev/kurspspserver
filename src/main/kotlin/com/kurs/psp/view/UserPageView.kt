package com.kurs.psp.view

import com.kurs.psp.controller.UserPageController
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.layout.BorderPane
import tornadofx.*

class UserPageView : SecureView("My View") {
    override val root: BorderPane by fxml("/view/userpage.fxml")
    private val userPageController: UserPageController by inject()

    private val label: Label by fxid("helloLabel")
    private val profileBtn: Button by fxid("profile")
    private val logoutBtn: Button by fxid("logout")
    private val scheduleBtn: Button by fxid("schedule")
    private val marksBtn: Button by fxid("marks")


    override fun onDock() {
        super.onDock()
        label.text = currentUser?.let { "Здравствуйте, ${it.surname} ${it.name}" } ?: "Здравствуйте, unknown"
    }

    init {
        scope.workspace.headingContainer.show()
        scope.workspace.header.show()
        profileBtn.setOnAction {
            userPageController.showProfile()
        }
        logoutBtn.setOnAction {
            currentUser = null
            userPageController.logout()
        }
        scheduleBtn.setOnAction {
            userPageController.showSchedule()
        }
        marksBtn.setOnAction {
            currentUser?.let {
                userPageController.showMarks(it.username)
            }
        }
    }
}
