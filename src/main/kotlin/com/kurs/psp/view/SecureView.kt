package com.kurs.psp.view

import com.kurs.psp.ClientSocket
import com.kurs.psp.model.User
import com.kurs.psp.utils.*
import com.psp.kurs.server.model.KursApi
import tornadofx.*

abstract class SecureView(title: String? = null) : View(title) {

    companion object {
        @JvmStatic
        var currentUser: User? = null
    }

    override fun onDock() {
        currentUser?.let { super.onCreate() }
            ?: preferences(PREFERENCE) {
                get(TOKEN, "")?.let { token ->
                    (ClientSocket.send(KursApi.SendLoginToken(token)) as KursApi.AuthenticationResult).let { response ->
                        if (response.isSuccess) {
                            ClientSocket.send(KursApi.GetUserByToken(token)).run {
                                if (this is KursApi.User)
                                    currentUser = User(
                                        username,
                                        surname,
                                        name,
                                        patronymic,
                                        age,
                                        groupNumber,
                                        EnglishLevel.valueOf(englishLevel),
                                        Education.valueOf(education),
                                        UserRoles.valueOf(role)
                                    )
                            }
                            super.onDock()
                        } else
                            workspace.navigateBack()
                    }
                } ?: workspace.navigateBack()
            }
    }
}
