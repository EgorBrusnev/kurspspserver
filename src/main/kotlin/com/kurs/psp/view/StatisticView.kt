package com.kurs.psp.view

import com.kurs.psp.controller.StatisticController
import com.kurs.psp.model.Mark
import com.kurs.psp.model.MarkList
import com.kurs.psp.model.User
import com.kurs.psp.utils.PREF_HEIGHT
import com.kurs.psp.utils.PREF_WIDTH
import com.psp.kurs.server.model.KursApi
import javafx.beans.property.ReadOnlyObjectWrapper
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.event.EventHandler
import javafx.scene.Parent
import javafx.scene.chart.CategoryAxis
import javafx.scene.chart.NumberAxis
import javafx.scene.control.ListView
import javafx.scene.control.TableColumn
import javafx.scene.control.TableView
import javafx.scene.control.cell.TextFieldTableCell
import javafx.util.converter.IntegerStringConverter
import tornadofx.*
import java.time.LocalDate

class StatisticView : SecureView("Статистика студентов") {

    private val statisticController: StatisticController by inject()

    private var markList = FXCollections.observableArrayList<MarkList>()
    private var userList = FXCollections.observableArrayList<User>()
    private val secondList = FXCollections.observableArrayList<MarkList>()
    private var dateFrom = SimpleObjectProperty<LocalDate>(LocalDate.now().minusDays(3))
    private var dateTo = SimpleObjectProperty<LocalDate>(LocalDate.now().plusDays(3))
    private var table = TableView<MarkList>()
    private var group = SimpleIntegerProperty()

    init {
        currentUser?.let {
            markList.clear()
            markList = if ("group" != params["type"]) {
                FXCollections.observableArrayList(
                    MarkList(
                        it.username,
                        statisticController.getMarksForUser(it.username)
                    )
                )

            } else {
                FXCollections.observableArrayList()
            }
        }
    }

    override val root = borderpane {
        prefHeight = PREF_HEIGHT
        prefWidth = PREF_WIDTH
        right {
            vbox {
                label("Group")
                textfield(group) {
                    action {
                        println("action")
                        markList.clear()
                        userList.clear()
                        userList.addAll(statisticController.getUsersForGroup(group.value))
                        markList.addAll(userList.takeIf { it.isNotEmpty() }?.let { users ->
                            statisticController.getMarksForGroup(group.value).let { marks ->
                                users.map { user ->
                                    MarkList(
                                        user.username,
                                        marks.filter { it.student == user.username })
                                }
                            }
                        } ?: emptyList())
                        table.refresh()
                    }
                }
                datepicker {
                    bind(dateFrom)
                    onAction = EventHandler {
                        table.repaint()
                    }
                }
                datepicker {
                    bind(dateTo)
                    onAction = EventHandler {
                        table.repaint()
                    }
                }
            }
        }
        center {
            table = tableview(markList) {
                isEditable = true
                repaint()
                columnResizePolicy = SmartResize.POLICY
            }
        }
    }

    private fun TableView<MarkList>.repaint() {
        columns.clear()
        column("Студент", MarkList::student).apply {
            minWidth(100)
            remainingWidth()
            setCellValueFactory {
                ReadOnlyObjectWrapper<String>("${it.value.student}")
            }
        }
        for (i in dateFrom.value..dateTo.value) {
            column(i.toString(), Int::class).apply {
                isEditable = true
                setCellValueFactory {
                    val m = markList.find { mark -> mark.student == it.value.student }?.run {
                        marks.find { mark -> mark.date.toString() == it.tableColumn.text }?.mark
                    }
                    SimpleObjectProperty<Int>(m)
                }
            }
        }
    }
}

operator fun ClosedRange<LocalDate>.iterator(): Iterator<LocalDate> {
    return object : Iterator<LocalDate> {

        private var next = this@iterator.start
        private val finalElement = this@iterator.endInclusive
        private var hasNext = !next.isAfter(this@iterator.endInclusive)

        override fun hasNext(): Boolean = hasNext

        override fun next(): LocalDate {
            val value = next
            if (value == finalElement) {
                hasNext = false
            } else {
                next = next.plusDays(1)
            }
            return value
        }
    }
}

class StatisticChart : SecureView() {
    override val root = borderpane {
        center {
            areachart("Average marks per week", CategoryAxis(), NumberAxis()) {
                series("English") {
                    data("MAR",100)
                    data("APR",200)
                    data("MAY",180)
                }
            }
        }
    }
}