package com.kurs.psp.view

import com.kurs.psp.controller.ProfileController
import com.kurs.psp.utils.PREF_HEIGHT
import com.kurs.psp.utils.PREF_WIDTH
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.Parent
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import tornadofx.*

class ProfileView : SecureView("Профиль") {

    private val profileController: ProfileController by inject()

    override val root: Parent = borderpane {
        prefHeight = PREF_HEIGHT
        prefWidth = PREF_WIDTH
        center {
            currentUser?.let {
                form {
                    fieldset {
                        field("Пользователь") {
                            textfield(it.usernameProperty) { }
                        }
                        field("Фамилия") {
                            textfield(it.surnameProperty) { }
                        }
                        field("Имя") {
                            textfield(it.nameProperty) { }
                        }
                        field("Отчество") {
                            textfield(it.patronymicProperty) {}
                        }
                        field("Возраст") {
                            textfield(it.ageProperty) {}
                        }
                        field("Группа") {
                            textfield(it.groupNumberProperty) {
                                disableWhen { SimpleBooleanProperty(true) }
                            }
                        }
                    }
                    button("Обновить") {
                        action {
                            profileController.updateUser(it)
                        }
                    }
                }
            } ?: label("Пожалуйста войдите в систему!")
        }
    }

    fun updateUser() {
        onDock()
    }
}
