package com.kurs.psp.view.admin

import com.kurs.psp.controller.AdminRequestsController
import com.kurs.psp.model.Group
import com.kurs.psp.model.User
import com.kurs.psp.utils.PREF_HEIGHT
import com.kurs.psp.utils.PREF_WIDTH
import com.kurs.psp.view.SecureView
import javafx.collections.FXCollections
import javafx.scene.control.ChoiceDialog
import javafx.scene.control.TableView
import javafx.scene.control.TextInputDialog
import tornadofx.*

class AdminRequestsView : SecureView("My View") {

    private val controller: AdminRequestsController by inject()

    private var users = controller.getRequests().observable()

    private var table = TableView<User>()

    private val groupDialog = ChoiceDialog<Int>().apply {
        title = "Добаления пользователя"
        headerText = "Введите номер группы"
        items.addAll(controller.getAllGroups().map { it.id })
    }

    override val root = borderpane {
        prefWidth = PREF_WIDTH
        prefHeight = PREF_HEIGHT
        center {
            table = tableview(users) {
                columnResizePolicy = SmartResize.POLICY
                column("Пользователь", User::username)
                column("Фамилия", User::surname)
                column("Имя", User::name)
                column("Отчество", User::patronymic)
                column("Возраст", User::age)
                column("Образование", User::education)
                column("Уровень английского", User::englishLevel)
            }
        }

        bottom {
            hbox {
                button("Принять") {
                    action {
                        table.selectedCell?.row?.let { row ->
                            groupDialog.showAndWait().takeIf { it.isPresent }
                                ?.let { users[row].apply { groupNumber = it.get() } }
                                ?.run { controller.setUserGroup(username, groupNumber) }
                        }
                    }
                }
            }
        }
    }

    fun refreshTable() {
        users.setAll(controller.getRequests().observable())
        table.refresh()
    }
}
