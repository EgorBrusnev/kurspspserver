package com.kurs.psp.view.admin

import com.kurs.psp.controller.AdminController
import com.kurs.psp.utils.PREF_HEIGHT
import com.kurs.psp.utils.PREF_WIDTH
import com.kurs.psp.view.*
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.text.Font
import tornadofx.*

class AdminMainView : SecureView("Администратор") {

    private val controller: AdminController by inject()
    private var helloLabel = Label()

    init {
        scope.workspace.headingContainer.show()
        scope.workspace.header.show()
    }

    override fun onDock() {
        super.onDock()
        helloLabel.text = "Здравствуйте, ${currentUser?.surname} ${currentUser?.name}"
    }

    override val root = vbox {
        prefWidth = PREF_WIDTH
        prefHeight = PREF_HEIGHT
        alignment = Pos.CENTER

        hbox {
            alignment = Pos.CENTER
            helloLabel = label {

                font = Font.font(24.0)
                hboxConstraints { margin = Insets(20.0) }
            }
        }
        hbox {
            alignment = Pos.CENTER
            button {
                prefWidth = 180.0
                prefHeight = 50.0
                label("Профиль")
                hboxConstraints { margin = Insets(5.0) }
                action {
                    workspace.dock<ProfileView>()
                }
            }
        }
        hbox {
            alignment = Pos.CENTER
            button {
                prefWidth = 180.0
                prefHeight = 50.0
                label("Заявки")
                hboxConstraints { margin = Insets(5.0) }
                action {
                    workspace.dock<AdminRequestsView>()
                }
            }
        }
        hbox {
            alignment = Pos.CENTER
            button {
                prefWidth = 180.0
                prefHeight = 50.0
                label("Пользователи")
                hboxConstraints { margin = Insets(5.0) }
                action {
                    workspace.dock<UserListView>()
                }
            }
        }
        hbox {
            alignment = Pos.CENTER
            button {
                prefWidth = 180.0
                prefHeight = 50.0
                label("Статистика")
                hboxConstraints { margin = Insets(5.0) }
                action {
                    workspace.dock<StatisticView>(params = *arrayOf(Pair("type", "group")))
                }
            }
        }
        hbox {
            alignment = Pos.CENTER
            button {
                prefWidth = 180.0
                prefHeight = 50.0
                label("Расписания")
                hboxConstraints { margin = Insets(5.0) }
                action {
                    workspace.dock<ScheduleView>()
                }
            }
        }
        hbox {
            alignment = Pos.CENTER
            button {
                prefWidth = 180.0
                prefHeight = 50.0
                label("Выйти")
                hboxConstraints { margin = Insets(5.0) }
                action {
                    currentUser = null
                    controller.logout()
                }
            }
        }
    }
}

