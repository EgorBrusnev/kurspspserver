package com.kurs.psp.view

import com.kurs.psp.controller.LoginController
import com.kurs.psp.login.Styles.Companion.loginScreen
import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.util.Duration
import tornadofx.*

class LoginView : View("Войти") {
    private val loginController: LoginController by inject()

    private val model = object : ViewModel() {
        val username = bind { SimpleStringProperty() }
        val password = bind { SimpleStringProperty() }
        val rememberMe = bind { SimpleBooleanProperty() }
    }

    override val root = form {
        addClass(loginScreen)
        alignment = Pos.CENTER
        fieldset {
            field("Пользователь") {
                textfield(model.username) {
                    required()
                    whenDocked { requestFocus() }
                }
            }
            field("Пароль") {
                passwordfield(model.password).required()
            }
            field("Запомни меня") {
                checkbox(property = model.rememberMe)
            }
        }

        button("Войти") {
            isDefaultButton = true
            action {
                model.commit {
                    loginController.tryLogin(
                        model.username.value,
                        model.password.value,
                        remember = model.rememberMe.value
                    )
                }
            }
        }
        hyperlink(text = "Регистрация") {
            action {
                loginController.showRegistration()
            }
        }
    }

    override fun onDock() {
        model.validate(decorateErrors = true)
    }

    fun shakeStage() {
        var x = 0
        var y = 0
        val cycleCount = 10
        val move = 10
        val keyframeDuration = Duration.seconds(0.04)

        val stage = FX.primaryStage

        val timelineX = Timeline(KeyFrame(keyframeDuration, EventHandler {
            if (x == 0) {
                stage.x = stage.x + move
                x = 1
            } else {
                stage.x = stage.x - move
                x = 0
            }
        }))

        timelineX.cycleCount = cycleCount
        timelineX.isAutoReverse = false

        val timelineY = Timeline(KeyFrame(keyframeDuration, EventHandler {
            if (y == 0) {
                stage.y = stage.y + move
                y = 1
            } else {
                stage.y = stage.y - move
                y = 0
            }
        }))

        timelineY.cycleCount = cycleCount
        timelineY.isAutoReverse = false

        timelineX.play()
        timelineY.play()
    }

    fun clear() {
        model.username.value = ""
        model.password.value = ""
        model.rememberMe.value = false
    }
}
